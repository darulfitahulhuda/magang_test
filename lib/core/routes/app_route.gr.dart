// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i4;
import 'package:magang/presentation/ui/change_color/change_color_view.dart'
    as _i1;
import 'package:magang/presentation/ui/home/home_view.dart' as _i2;
import 'package:magang/presentation/ui/main/main_view.dart' as _i3;

abstract class $AppRouter extends _i4.RootStackRouter {
  $AppRouter({super.navigatorKey});

  @override
  final Map<String, _i4.PageFactory> pagesMap = {
    ChangeColorView.name: (routeData) {
      return _i4.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i1.ChangeColorView(),
      );
    },
    HomeView.name: (routeData) {
      return _i4.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i2.HomeView(),
      );
    },
    MainView.name: (routeData) {
      return _i4.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i3.MainView(),
      );
    },
  };
}

/// generated route for
/// [_i1.ChangeColorView]
class ChangeColorView extends _i4.PageRouteInfo<void> {
  const ChangeColorView({List<_i4.PageRouteInfo>? children})
      : super(
          ChangeColorView.name,
          initialChildren: children,
        );

  static const String name = 'ChangeColorView';

  static const _i4.PageInfo<void> page = _i4.PageInfo<void>(name);
}

/// generated route for
/// [_i2.HomeView]
class HomeView extends _i4.PageRouteInfo<void> {
  const HomeView({List<_i4.PageRouteInfo>? children})
      : super(
          HomeView.name,
          initialChildren: children,
        );

  static const String name = 'HomeView';

  static const _i4.PageInfo<void> page = _i4.PageInfo<void>(name);
}

/// generated route for
/// [_i3.MainView]
class MainView extends _i4.PageRouteInfo<void> {
  const MainView({List<_i4.PageRouteInfo>? children})
      : super(
          MainView.name,
          initialChildren: children,
        );

  static const String name = 'MainView';

  static const _i4.PageInfo<void> page = _i4.PageInfo<void>(name);
}
