import 'package:auto_route/auto_route.dart';

import 'app_route.gr.dart';

@AutoRouterConfig()
class AppRouter extends $AppRouter {
  @override
  List<AutoRoute> get routes => [
        /// This main is initial Route
        AutoRoute(page: MainView.page, initial: true, path: Routes.main),

        AutoRoute(page: HomeView.page, path: Routes.home),
        AutoRoute(page: ChangeColorView.page, path: Routes.changeColor),
      ];
}

class Routes {
  static const main = "/";
  static const home = "/home";
  static const changeColor = "/change-color";
}
