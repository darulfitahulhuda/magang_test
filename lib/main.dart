import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:magang/core/di/di.dart' as di;
import 'package:magang/presentation/bloc/bloc/news_bloc.dart';

import 'core/di/di.dart';
import 'core/routes/app_route.dart';

void main() async {
  await di.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    final AppRouter appRouter = AppRouter();

    return BlocProvider(
      create: (context) => sl<NewsBloc>()..add(const NewsEvent.getNews()),
      child: MaterialApp.router(
        title: 'Magang Test',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
          useMaterial3: true,
        ),
        // routerDelegate: appRouter.delegate(),
        // routeInformationParser: appRouter.defaultRouteParser(),
        routerConfig: appRouter.config(),
        // home: const HomeView(),
      ),
    );
  }
}
