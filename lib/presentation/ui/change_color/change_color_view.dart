import 'package:auto_route/annotations.dart';
import 'package:flutter/material.dart';

@RoutePage()
class ChangeColorView extends StatefulWidget {
  const ChangeColorView({super.key});

  @override
  State<ChangeColorView> createState() => _ChangeColorViewState();
}

class _ChangeColorViewState extends State<ChangeColorView> {
  /// All colors
  final colors = [
    Colors.green,
    Colors.black,
    Colors.amber,
  ];

  int index = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Change Color"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              height: 150,
              width: 150,
              decoration: BoxDecoration(
                color: colors[index],
                borderRadius: BorderRadius.circular(20),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 40),
              child: ElevatedButton(
                onPressed: () {
                  if (index == 2) {
                    index = 0;
                  } else {
                    index++;
                  }
                  setState(() {});
                },
                style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10))),
                child: const Text("Ganti Warna"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
